# Thesaurus

## Compiling and executing:

First change the permissions:

```
chmod 755 run
```

And then:

```
> ./run exemplo.th
```

OR:

```
> make
```

```
> ./thesaurus < exemplo.th
```

```
> make dots
```

```
> open out/html/index.html
```

## Cleaning:

```
> make clean
```
